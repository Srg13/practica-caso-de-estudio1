//Samantha Roldán García
//27/03/2020

#include "edth.h"
#include "edth.c"

int main(int argc, char *argv[])
{
    const int C_files=5;
    const int C_columns=15;
    const int C_windows=3;
    char array[C_files];
    char array2[C_files][C_columns];
    int appData[4];
    appData[0]=C_windows;
    appData[1]=C_files;
    appData[2]=C_columns;
    appData[3]=0;
    int appCursors[appData[0]][2];//Posición del cursor para cada ventana(y,x)
    char windows[appData[0]][5][15];
    iniWindows(appData,windows,appCursors);
    
    parseInput(appData,windows, appCursors, argv [1]);
    printCurrentWindow(appData,windows,appCursors);
    /*//Ejecución 1
    printCurrentWindow(appData,windows,appCursors);
    parseInput(appData,windows,appCursors,"jjliHola Mudno");
    printCurrentWindow(appData,windows,appCursors);
    parseInput(appData,windows,appCursors,"&hhhxlid");
    printCurrentWindow(appData,windows,appCursors);
    //Ejecución 2
    parseInput(appData,windows,appCursors,"&g");
    printCurrentWindow(appData,windows,appCursors);
    parseInput(appData,windows,appCursors,"DGiFin");
    printCurrentWindow(appData,windows,appCursors);
    parseInput(appData,windows,appCursors,"&kk0iuno dos tres&");
    printCurrentWindow(appData,windows,appCursors);
    parseInput(appData,windows,appCursors,"0wW");
    printCurrentWindow(appData,windows,appCursors);*/
}
