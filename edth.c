//Samantha Roldán García
//27/03/2020

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "edth.h"

#define INI ' '

//3.1
void iniEmptyArray(int columns, char array[])
{
    for(int i = 0; i<=columns-1; i++)
    {
        array[i]= INI;
    }
}
void printArray(int columns, char array[])
{
    for(int i=0; i<=columns-1; i++)
    {
        printf("%c", array[i]);
    }
}
void putCharOnArray(int columns, char array[], int column, char c)
{
    //el put substituye un espacio en blanco por un carácter
    if(column < columns)
    {
        array[column]=c;
    }
}
void shiftRightArray(int columns, char array[], int column)
{
    int i= columns-1;
    while(i>column)
    {
        array[i]=array[i-1];
        i--;
    }
}
void shiftLeftArray(int columns, char array[], int column)
{
    //tiene que borrar cuando desplaza
    int i=column;
    while(i<columns)
    {
        array[i]=array[i+1];
        i++;
    }
    array[columns-1] = INI;
}
//3.2
void iniEmptyMatrix(int files, int columns, char matrix[][15])
{
    for(int i=0; i<=files-1; i++)
    {
        iniEmptyArray(columns, matrix[i]);
    }
}
void printMatrix(int files, int columns, char matrix[][15])
{
    for (int i=0; i<=files-1; i++)
    {
        printf("%2i|",i);
        printArray(columns,matrix[i]);
        printf("|\n");
    }
}
void insertCharOnMatrix(int files, int columns, char matrix[][15], int file, int column, char c)
{
    insertCharOnArray(columns,matrix[file],column,c);
}
void deleteCharOnMatrix(int files, int columns, char matrix[][15], int file, int column)
{
    //borrar un carácter de la matriz
    shiftLeftArray(columns,matrix[file],column);
}
//3.3
void insertCharOnArray(int columns, char array[], int column, char c)
{
    //el insert inserta un carácter y desplaza el texto a un lado
    shiftRightArray(columns,array,column);
    if(column==columns)
    {
        shiftLeftArray(columns,array,column);
        column-=1;
    }
    putCharOnArray(columns,array,column,c);
}
void setActiveWindow(int appData[], int aw)
{
    appData[3]=aw;
}
void iniWindow(int appData[],char windows[][appData[1]][appData[2]],int window)
{
    iniEmptyMatrix(appData[1],appData[2],windows[window]);
}
void iniWindows(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    appCursors[appData[3]][0] = 0;
    appCursors[appData[3]][1] = 0;
    iniEmptyMatrix(appData[1],appData[2],windows[appData[3]]);
}
void printWindowInfo(int appData[],int appCursors[][2])
{
    printf("%d -%d , %d",appData[3],appCursors[appData[3]][0],appCursors[appData[3]][1]);
}
void printWindow(int appData[], char matrix[][appData[2]])
{
    printf("\n");
    printMatrix(appData[1], appData[2], matrix);
}
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{
    printWindowInfo(appData,appCursors);
    printWindow(appData,windows[appData[3]]);
}
bool isLegalFile(int appData[],int y)
{
    if(y<appData[1] && y>=0)
    {
        return true;
    }
    return false;
}
void setYCursorOnWindow(int appData[],int appCursors[][2],int y)
{
    if(isLegalFile(appData,y))
    {
        appCursors[appData[3]][0] = y;
    }
}
bool isLegalColumn(int appData[], int x)
{
    if(x<appData[2] && x>=0)
    {
        return true;
    }
    return false;
}
void setXCursorOnWindow(int appData[],int appCursors[][2],int x)
{
    if(isLegalColumn(appData,x))
    {
        appCursors[appData[3]][1] = x;
    }
}
//3.4
void insertCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char c)
{
    insertCharOnMatrix(appData[1],appData[2],windows[appData[3]],appCursors[appData[3]][0],appCursors[appData[3]][1],c);
}
void deleteCurrentPositionOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    deleteCharOnMatrix(appData[1],appData[2],windows[appData[3]],appCursors[appData[3]][0],appCursors[appData[3]][1]);
}
void deleteCurrentFileOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    for (int i= appCursors[appData[3]][0]+1; i < appData[1]; i++)
    {
        for(int x= 0; x<appData[2];x++)
        {
            insertCharOnMatrix(appData[1],appData[2],windows[appData[3]],i-1,x,windows[appData[3]][i][x]);
        }
    }
}

//AVANZADO
int searchCharOnArray(int columns, char array[], int column, char c)
{
    for(int i=column; i<=columns-1; i++)
    {
        if(array[i] == c)
        {
            return i;
        }
    }
    return columns;
}
int searchNoCharOnArray(int columns, char array[], int column, char c)
{
    for(int i=column; i<=columns-1; i++)
    {
        if(array[i] != c)
        {
            return i;
        }
    }
    return 0;
}
int searchCharOnMatrix(int files, int columns, char matrix[files][columns],int file, int column, char c)
{
    int search = 0;
    for(int i=file; i<=files-1; i++)
    {
        search = searchCharOnArray(columns,matrix[i],column,c);
        if(search < columns)
        {
            break;
        }
    }
    return search;
}
int searchNoCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c)
{
    int search = 0;
    for(int i=file;i<=files-1;i++)
    {
        search = searchNoCharOnArray(columns,matrix[i],column,c);
        if(search==0)
        {
            break;
        }
    }
    return search;
}
int searchCharOnWindow(int appData[], char window[][appData[1]][appData[2]], int appCursors[][2], char c)
{
    return searchCharOnMatrix(appData[1],appData[2],window[appData[3]],appCursors[appData[3]][0],appCursors[appData[3]][1],c);
}
//Parser
void parser(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2], char input)
{
    static int mode=0;
    if(mode == 0)
    {
        switch(input)
        {
            case 'h':
                moveCursorRight(appData,appCursors);
                break;
            case 'l':
                moveCursorLeft(appData,appCursors);
                break;
            case 'j':
                moveCursorDown(appData,appCursors);
                break;
            case 'k':
                moveCursorUp(appData,appCursors);
                break;
            case 'g':
                gotoFirstFile(appData,appCursors);
                break;
            case 'G':
                gotoLastFile(appData,appCursors);
                break;
            case '0':
                gotoBeginLine(appData,appCursors);
                break;
            case 'w':
                gotoNextWord(appData,windows,appCursors);
                break;
            case 'W':
                deleteWord(appData,windows,appCursors);
                break;
            case 'D':
                deleteCurrentFile(appData,windows,appCursors);
                break;
            case 'o':
                moveCursorDown(appData,appCursors);
                mode=1;
                break;
            case 'O':
                moveCursorUp(appData,appCursors);
                mode=1;
                break;
            case 'i':
                mode=1;
                break;
            case 'x':
                deleteCurrentPosition(appData,windows,appCursors);
                break;
            case 't':
                if(appData[3]== appData[0]-1)
                {
                    setActiveWindow(appData,0);
                }
                else
                {
                    setActiveWindow(appData,appData[3]+1);
                }
                iniWindows(appData, windows,appCursors);
                break;
            case 'T':
                if(appData[3]== 0)
                {
                    setActiveWindow(appData,appData[0]-1);
                }
                else
                {
                    setActiveWindow(appData,appData[3]-1);
                }
                iniWindows(appData, windows,appCursors);
                break;
        }
    }else if(mode == 1)
    {
        switch(input)
        {
            case '&':
                mode=0;
                break;
            default:
                intCharOnWindow(appData, windows, appCursors, input);
                moveCursorLeft(appData,appCursors);
        }
    }
}
void parseInput(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2], char input[])
{
    for(int i=0;i<strlen(input);i++)
    {
        parser(appData,windows,appCursors,input[i]);
    }
}
//Funciones de movimiento del cursor y edición de texto
void gotoBeginLine(int appData[],int appCursors[][2])
{
    setXCursorOnWindow(appData,appCursors,0);
}
void moveCursorRight(int appData[],int appCursors[][2])
{
    setXCursorOnWindow(appData,appCursors,appCursors[appData[3]][1]-1);
}
void moveCursorLeft(int appData[],int appCursors[][2])
{
    setXCursorOnWindow(appData,appCursors,appCursors[appData[3]][1]+1);
}
void moveCursorDown(int appData[],int appCursors[][2])
{
    setYCursorOnWindow(appData,appCursors,appCursors[appData[3]][0]+1);
}
void moveCursorUp(int appData[],int appCursors[][2])
{
    setYCursorOnWindow(appData,appCursors,appCursors[appData[3]][0]-1);
}
void gotoFirstFile(int appData[],int appCursors[][2])
{
    setYCursorOnWindow(appData,appCursors,0);
}
void gotoLastFile(int appData[],int appCursors[][2])
{
    setYCursorOnWindow(appData,appCursors,appData[1]-1);
}
void deleteCurrentPosition(int appData[],char window[][appData[1]][appData[2]],int appCursors[][2])
{
    deleteCurrentPositionOnWindow(appData,window,appCursors);
}
void gotoNextWord(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    int posicion= searchCharOnWindow(appData,windows,appCursors,INI);
    if(posicion<appData[2])
    {
        int appCursorsTmp[appData[0]][2];
        appCursorsTmp[appData[3]][0]=appCursors[appData[3]][0];
        appCursorsTmp[appData[3]][1]=posicion;
        posicion=searchCharOnWindow(appData,windows,appCursorsTmp,INI);
        if(posicion<appData[2])
        {
            appCursors[appData[3]][1] = posicion + 1;
        }
    }
}
void deleteWord(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    int posicion =searchCharOnWindow(appData,windows,appCursors, INI);
    if(posicion<appData[2])
    {
        int appCursorsTmp[appData[0]][2];
        appCursorsTmp[appData[3]][0]=appCursors[appData[3]][0];
        appCursorsTmp[appData[3]][1]=posicion;
        posicion=searchCharOnWindow(appData,windows,appCursorsTmp, INI);
    }
    if(posicion==appData[2])
    {
        posicion=appData[2]-1;
    }
    for(int i=appCursors[appData[3]][1];i<posicion;i++)
    {
        deleteCurrentPosition(appData,windows,appCursors);
    }
}
void deleteCurrentFile(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2])
{
    deleteCurrentFileOnWindow(appData,windows,appCursors);
}
void intCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char input)
{
    insertCharOnMatrix(appData[1],appData[2],windows[appData[3]],appCursors[appData[3]][0],appCursors[appData[3]][1],input);
}
