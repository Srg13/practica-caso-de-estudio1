//Samantha Roldán García
//27/03/2020

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define INI ' '

//3.1
void iniEmptyArray(int columns, char array[]);
void printArray(int columns, char array[]);
void putCharOnArray(int columns, char array[], int column, char c);
void shiftRightArray(int columns, char array[], int column);
void shiftLeftArray(int columns, char array[], int column);
void insertCharOnArray(int columns, char array[], int column, char c);

//3.2
void iniEmptyMatrix(int files, int columns, char matrix[][15]);
void printMatrix(int files, int columns, char matrix[][15]);
void insertCharOnMatrix(int files, int columns, char matrix[][15], int file, int column, char c);
void deleteCharOnMatrix(int files, int columns, char matrix[][15], int file, int column);

//3.3
void setActiveWindow(int appData[], int aw);
void iniWindow(int appData[],char windows[][appData[1]][appData[2]],int window);
void iniWindows(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void printWindowInfo(int appData[],int appCursors[][2]);
void printWindow(int appData[], char matrix[][appData[2]]);
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
bool isLegalFile(int appData[],int y);
void setYCursorOnWindow(int appData[],int appCursors[][2],int y);
bool isLegalColumn(int appData[], int x);
void setXCursorOnWindow(int appData[],int appCursors[][2],int x);

//3.4
void insertCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char c);
void deleteCurrentPositionOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void deleteCurrentFileOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);

//AVANZADO
//Search
int searchCharOnArray(int columns, char array[], int column, char c);
int searchNoCharOnArray(int columns, char array[], int column, char c);
int searchCharOnMatrix(int files, int columns, char matrix[files][columns],int file, int column, char c);
int searchNoCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c);
int searchCharOnWindow(int appData[], char window[][appData[1]][appData[2]], int appCursors[][2], char c);

//Parser
void parser(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2], char input);

//Parser input
void parseInput(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2], char input[]);

//Funciones de movimiento del cursor y de edición de texto
void moveCursorRight(int appData[],int appCursors[][2]);
void moveCursorLeft(int appData[],int appCursors[][2]);
void moveCursorDown(int appData[],int appCursors[][2]);
void moveCursorUp(int appData[],int appCursors[][2]);
void gotoFirstFile(int appData[],int appCursors[][2]);
void gotoLastFile(int appData[],int appCursors[][2]);
void gotoBeginLine(int appData[],int appCursors[][2]);
void gotoNextWord(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void deleteCurrentPosition(int appData[],char window[][appData[1]][appData[2]],int appCursors[][2]);
void deleteWord(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void deleteCurrentFile(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2]);
void intCharOnWindow(int appData[],char windows[][appData[1]][appData[2]],int appCursors[][2],char input);


