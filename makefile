all: PracticaCDE1
.PHONY: test

resultados = resultados
caso1 = "jjjklllhiEsto es un texto"
caso2 = "ttTiEscribo&jjllliTexto&g"
caso3 = "GiSon varias palabras&hhxxial&hhhx"
caso4 = "jjia b c d e f g h&hhhhwwiz"
caso5 = "ooOiLinea palabra&jLinea&DkhhhW"

PracticaCDE1: PracticaCDE1.o
	gcc -o PracticaCDE1 PracticaCDE1.o
PracticaCDE1.o: PracticaCDE1.c
	gcc -c PracticaCDE1.c edth.h
edth: edth.o
	gcc -o edth edth.o
edth.o: edth.c edth.o
	gcc -c edth.c

test: PracticaCDE1

	@echo "Comprobando casos"
	@echo
	@echo "Caso 1"
	@./PracticaCDE1 $(caso1) > $(resultados)/PracticaCDE1$(caso1).txt 
	@diff -c $(resultados)/PracticaCDE1$(caso1).txt $(resultados)/PracticaCDE1$(caso1).txt	
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 2"
	@./PracticaCDE1 $(caso2) > $(resultados)/PracticaCDE1$(caso2).txt 
	@diff -c $(resultados)/PracticaCDE1$(caso2).txt $(resultados)/PracticaCDE1$(caso2).txt	
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 3"
	@./PracticaCDE1 $(caso3) > $(resultados)/PracticaCDE1$(caso3).txt 
	@diff -c $(resultados)/PracticaCDE1$(caso3).txt $(resultados)/PracticaCDE1$(caso3).txt	
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 4"
	@./PracticaCDE1 $(caso4) > $(resultados)/PracticaCDE1$(caso4).txt 
	@diff -c $(resultados)/PracticaCDE1$(caso4).txt $(resultados)/PracticaCDE1$(caso4).txt	
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 5"
	@./PracticaCDE1 $(caso5) > $(resultados)/PracticaCDE1$(caso5).txt 
	@diff -c $(resultados)/PracticaCDE1$(caso5).txt $(resultados)/PracticaCDE1$(caso5).txt	
	@echo "Test realizado correctamente"
	@echo
	
clean:
	rm *.o PracticaCDE1 
